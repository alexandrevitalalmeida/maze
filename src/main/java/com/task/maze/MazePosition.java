package com.task.maze;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
@Getter
public class MazePosition {

    private final int row;
    private final int column;

    public int getNextRow(DirectionType directionType) {
        if (DirectionType.UP.equals(directionType)) {
            return row - 1;
        } else if (DirectionType.DOWN.equals(directionType)) {
            return row + 1;
        }
        return row;
    }

    public int getNextColumn(DirectionType directionType) {
        if (DirectionType.RIGHT.equals(directionType)) {
            return column + 1;
        } else if (DirectionType.LEFT.equals(directionType)) {
            return column - 1;
        }
        return column;
    }
}