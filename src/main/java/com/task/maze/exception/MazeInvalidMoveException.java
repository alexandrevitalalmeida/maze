package com.task.maze.exception;

public class MazeInvalidMoveException extends RuntimeException {

    public MazeInvalidMoveException(String message) {
        super(message);
    }
}
