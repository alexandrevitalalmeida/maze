package com.task.maze;

import java.util.Arrays;
import java.util.Optional;

public enum DirectionType {
    UP(0),
    RIGHT(1),
    DOWN(2),
    LEFT(3);

    private final int value;

    DirectionType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Optional<DirectionType> getByValue(int value) {
        return Arrays.stream(DirectionType.values())
                .filter(type -> type.getValue() == value)
                .findFirst();
    }
}