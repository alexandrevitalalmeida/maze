package com.task.maze;

import com.task.maze.exception.MazeInvalidMoveException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class Maze implements IMaze {

    private final List<List<MazeCellType>> myMaze;

    private DirectionType currentDirection = DirectionType.UP;
    private MazePosition currentExplorerPosition;
    private boolean mazeCompleted = false;

    @Override
    public MazeCellType getCellTypeByCoordinates(int row, int column) {
        try {
            return this.myMaze.get(row).get(column);
        } catch (IndexOutOfBoundsException e) {
            return MazeCellType.NON_EXISTENT;
        }
    }

    @Override
    public long getQuantity(MazeCellType cellType) {
        return this.myMaze.stream()
                .flatMap(List::stream)
                .filter(cellType::equals).count();
    }

    @Override
    public Maze turnRight() {
        int nextValueDirection = this.currentDirection.getValue() + 1;
        this.currentDirection = DirectionType.getByValue(nextValueDirection)
                .orElse(DirectionType.UP);
        return this;
    }

    @Override
    public Maze turnLeft() {
        int previousValueDirection = this.currentDirection.getValue() - 1;
        this.currentDirection = DirectionType.getByValue(previousValueDirection)
                .orElse(DirectionType.LEFT);
        return this;
    }

    @Override
    public Maze moveForward() throws MazeInvalidMoveException {
        boolean canMove = canMove(currentDirection);
        if (canMove) {
            moveToNextPosition();
        } else {
            MazePosition nextPosition = getExplorerNextPosition(currentDirection);
            MazeCellType cellType = getCellTypeByCoordinates(nextPosition.getRow(), nextPosition.getColumn());
            if (MazeCellType.EXIT.equals(cellType)) {
                mazeCompleted = true;
                return this;
            }
            String message = "Cannot move forward to the next position - The next position is " + cellType.getName();
            log.error(message);
            throw new MazeInvalidMoveException(message);
        }
        return this;
    }

    public DirectionType getCurrentDirection() {
        return currentDirection;
    }

    public boolean isMazeCompleted() {
        return mazeCompleted;
    }

    private void moveToNextPosition() {
        log.info("moving explorer to the next position");
        MazePosition nextPosition = getExplorerNextPosition(currentDirection);
        MazePosition currentPosition = getExplorerCurrentPosition();
        setCellType(currentPosition, MazeCellType.FREE_SPACE);
        setCellType(nextPosition, MazeCellType.EXPLORER);
        setExplorerCurrentPosition();
    }

    private void setCellType(MazePosition position, MazeCellType cellType) {
        myMaze.get(position.getRow()).set(position.getColumn(), cellType);
    }

    private boolean canMove(DirectionType directionType) {
        MazePosition nextPosition = getExplorerNextPosition(directionType);
        return MazeCellType.FREE_SPACE.equals(
                getCellTypeByCoordinates(nextPosition.getRow(), nextPosition.getColumn()));
    }

    private MazePosition getExplorerNextPosition(DirectionType directionType) {
        MazePosition mazePosition = getExplorerCurrentPosition();
        return new MazePosition(mazePosition.getNextRow(directionType), mazePosition.getNextColumn(directionType));
    }

    private MazePosition getExplorerCurrentPosition() {
        if (currentExplorerPosition != null) {
            MazeCellType cellType = myMaze.get(currentExplorerPosition.getRow())
                    .get(currentExplorerPosition.getColumn());
            if (MazeCellType.EXPLORER.equals(cellType)) {
                return currentExplorerPosition;
            }
        }
        setExplorerCurrentPosition();
        return currentExplorerPosition;
    }

    private void setExplorerCurrentPosition() {
        for (int row = 0; row < myMaze.size(); row++) {
            int column = myMaze.get(row).indexOf(MazeCellType.EXPLORER);
            if (column != -1) {
                currentExplorerPosition = new MazePosition(row, column);
                break;
            }
        }
        log.info("Current explorer position is: " + currentExplorerPosition.toString());
    }
}