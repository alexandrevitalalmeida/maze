package com.task.maze;

import java.util.Arrays;
import java.util.Optional;

public enum MazeCellType {

    WALL("X", "Wall"),
    FREE_SPACE(" ", "Free space"),
    EXIT("F", "Exit"),
    EXPLORER("S", "Explorer"),
    NON_EXISTENT("", "Non existent");

    private final String value;
    private final String name;

    MazeCellType(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static Optional<MazeCellType> getByValue(String value) {
        return Arrays.stream(MazeCellType.values())
                .filter(type -> type.getValue().equals(value))
                .findFirst();
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
