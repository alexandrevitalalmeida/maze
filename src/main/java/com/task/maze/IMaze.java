package com.task.maze;


import com.task.maze.exception.MazeInvalidMoveException;

public interface IMaze {

    MazeCellType getCellTypeByCoordinates(int row, int column);

    long getQuantity(MazeCellType cellType);

    Maze turnRight();

    Maze turnLeft();

    Maze moveForward() throws MazeInvalidMoveException;
}
