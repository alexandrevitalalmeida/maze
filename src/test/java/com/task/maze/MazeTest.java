package com.task.maze;


import com.task.maze.exception.MazeInvalidMoveException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.task.maze.MazeCellType.EXIT;
import static com.task.maze.MazeCellType.EXPLORER;
import static com.task.maze.MazeCellType.FREE_SPACE;
import static com.task.maze.MazeCellType.NON_EXISTENT;
import static com.task.maze.MazeCellType.WALL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(JUnitParamsRunner.class)
public class MazeTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void given_validMaze_when_movementsAreDoneCorrectly_then_completeMaze() {
        // Given
        List<List<MazeCellType>> validMazeList = getValidMazeList();
        Maze maze = new Maze(validMazeList);

        // When
        maze.turnRight()
                .moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward()
                .turnRight()
                .moveForward().moveForward().moveForward()
                .turnRight()
                .moveForward().moveForward().moveForward().moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward()
                .turnRight()
                .moveForward().moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward()
                .turnRight()
                .moveForward()
                .turnLeft()
                .moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward()
                .turnLeft()
                .moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward().moveForward();

        assertThat(maze.isMazeCompleted()).isFalse();
        maze.moveForward();

        // Then
        boolean mazeCompleted = maze.isMazeCompleted();
        assertThat(mazeCompleted).isTrue();
    }

    @Test
    @Parameters(method = "paramsFor_given_validMaze_when_getCellTypeByCoordinatesIsInvoked_then_returnCorrectMazeCellType")
    public void given_validMaze_when_getCellTypeByCoordinatesIsInvoked_then_returnCorrectMazeCellType(int row, int column, MazeCellType expectedCellType) {
        // Given
        List<List<MazeCellType>> myMaze = getValidMazeList();
        Maze maze = new Maze(myMaze);

        // When
        MazeCellType actualCellType = maze.getCellTypeByCoordinates(row, column);

        // Then
        assertThat(actualCellType).isEqualTo(expectedCellType);
    }

    private Object[] paramsFor_given_validMaze_when_getCellTypeByCoordinatesIsInvoked_then_returnCorrectMazeCellType() {
        return new Object[]{
                // row | column | expectedCellType
                new Object[]{14, 1, EXIT},
                new Object[]{1, 1, FREE_SPACE},
                new Object[]{0, 0, WALL},
                new Object[]{3, 3, EXPLORER},
                new Object[]{-1, -1, NON_EXISTENT}
        };
    }

    @Test
    @Parameters(method = "paramsFor_given_cellType_when_getQuantityIsInvoked_then_returnCorrectNumberOfGivenCellType")
    public void given_cellType_when_getQuantityIsInvoked_then_returnCorrectNumberOfGivenCellType(MazeCellType cellType, long expectedQuantity) {
        // Given
        List<List<String>> myMaze = new ArrayList<>();
        myMaze.add(Arrays.asList("X", "X", " ", "X", "X", "X", "X"));
        myMaze.add(Arrays.asList("X", "X", " ", " ", " ", "X", "X"));
        myMaze.add(Arrays.asList("X", "X", "X", "X", " ", "X", "X"));
        myMaze.add(Arrays.asList("X", "X", "X", "X", "S", "X", "X"));
        myMaze.add(Arrays.asList("X", "X", "X", "X", "F", "X", "X"));
        Maze maze = new Maze(toObjectBasedMazeList(myMaze));

        // When
        long quantity = maze.getQuantity(cellType);

        // Then
        assertThat(quantity).isEqualTo(expectedQuantity);
    }

    private Object[] paramsFor_given_cellType_when_getQuantityIsInvoked_then_returnCorrectNumberOfGivenCellType() {
        return new Object[]{
                // cellType | expectedQuantity
                new Object[]{WALL, 28},
                new Object[]{FREE_SPACE, 5},
                new Object[]{EXIT, 1},
                new Object[]{EXPLORER, 1}
        };
    }

    @Test
    public void given_initialDirection_when_currentDirectionIsInvoked_then_isFacingUp() {
        // Given
        Maze maze = new Maze(Collections.emptyList());

        // When
        DirectionType currentDirection = maze.getCurrentDirection();

        // Then
        assertThat(currentDirection).isEqualTo(DirectionType.UP);
    }

    @Test
    public void given_validMaze_when_turnRightIsInvoked_then_directionOfExplorerShouldBeReflected() {
        // Given
        List<List<MazeCellType>> validMazeList = getValidMazeList();
        Maze maze = new Maze(validMazeList);

        // When - Then
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
        maze.turnRight();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.RIGHT);
        maze.turnRight();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.DOWN);
        maze.turnRight();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.LEFT);
        maze.turnRight();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
    }

    @Test
    public void given_validMaze_when_turnLeftIsInvoked_then_directionOfExplorerShouldBeReflected() {
        // Given
        List<List<MazeCellType>> validMazeList = getValidMazeList();
        Maze maze = new Maze(validMazeList);

        // When - Then
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
        maze.turnLeft();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.LEFT);
        maze.turnLeft();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.DOWN);
        maze.turnLeft();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.RIGHT);
        maze.turnLeft();
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
    }

    @Test
    public void given_nextCellIsFree_when_movingForwardIsInvoked_then_moveIsAllowed() {
        // Given
        List<List<MazeCellType>> validMazeList = getValidMazeList();
        Maze maze = new Maze(validMazeList);

        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
        assertThat(maze.getCellTypeByCoordinates(3, 3)).isEqualTo(EXPLORER);
        assertThat(maze.getCellTypeByCoordinates(3, 4)).isEqualTo(FREE_SPACE);
        maze.turnRight();

        // When
        maze.moveForward();

        // Then
        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.RIGHT);
        assertThat(maze.getCellTypeByCoordinates(3, 3)).isEqualTo(FREE_SPACE);
        assertThat(maze.getCellTypeByCoordinates(3, 4)).isEqualTo(EXPLORER);
    }

    @Test
    public void given_nextCellIsAWall_when_movingForwardIsInvoked_then_throwException() {
        // Given
        List<List<MazeCellType>> validMazeList = getValidMazeList();
        Maze maze = new Maze(validMazeList);

        assertThat(maze.getCurrentDirection()).isEqualTo(DirectionType.UP);
        assertThat(maze.getCellTypeByCoordinates(3, 3)).isEqualTo(EXPLORER);
        assertThat(maze.getCellTypeByCoordinates(3, 4)).isEqualTo(FREE_SPACE);

        // When - Then
        assertThatThrownBy(maze::moveForward)
                .isInstanceOf(MazeInvalidMoveException.class)
                .hasMessage("Cannot move forward to the next position - The next position is Wall");
    }

    @Test
    public void mustBeAbleToDeclareWhatIsInFrontOfTheExplorer() {
        // TODO
    }

    @Test
    public void mustBeAbleToDeclareAllMovementOptionsFromCurrentLocation() {
        // TODO
    }

    @Test
    public void provideCorrectCoordinatesOfWhereTheExplorerHasBeen() {
        // TODO
    }

    private List<List<String>> buildValidMazeList() {
        List<List<String>> myMaze = new ArrayList<>();
        myMaze.add(Arrays.asList("X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"));
        myMaze.add(Arrays.asList("X", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "S", " ", " ", " ", " ", " ", " ", " ", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", " ", " ", " ", " ", " ", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", " ", "X", "X", "X", "X", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", " ", "X", "X", "X", "X", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", " ", " ", " ", " ", "X", "X", "X", "X", "X", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", " ", " ", " ", " ", " ", " ", " ", " ", " ", "X", " ", "X"));
        myMaze.add(Arrays.asList("X", " ", "X", "X", "X", "X", "X", "X", "X", "X", "X", " ", " ", " ", "X"));
        myMaze.add(Arrays.asList("X", "F", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"));
        return myMaze;
    }

    private List<List<MazeCellType>> getValidMazeList() {
        return toObjectBasedMazeList(buildValidMazeList());
    }

    private List<List<MazeCellType>> toObjectBasedMazeList(List<List<String>> maze) {
        return maze.stream()
                .map(rowList -> rowList.stream()
                        .map(value -> MazeCellType.getByValue(value).get())
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }
}
