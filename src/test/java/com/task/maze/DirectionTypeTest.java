package com.task.maze;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static com.task.maze.DirectionType.DOWN;
import static com.task.maze.DirectionType.LEFT;
import static com.task.maze.DirectionType.RIGHT;
import static com.task.maze.DirectionType.UP;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DirectionTypeTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    @Parameters(method = "paramsFor_given_validDirectionType_when_getByValueIsInvoked_then_returnEnum")
    public void given_validDirectionType_when_getByValueIsInvoked_then_returnEnum(int value, DirectionType expectedDirectionType) {
        // When
        Optional<DirectionType> result = DirectionType.getByValue(value);

        // Then
        assertThat(result).contains(expectedDirectionType);
    }

    private Object[] paramsFor_given_validDirectionType_when_getByValueIsInvoked_then_returnEnum() {
        return new Object[]{
                // value | expectedDirectionType
                new Object[]{0, UP},
                new Object[]{1, RIGHT},
                new Object[]{2, DOWN},
                new Object[]{3, LEFT}
        };
    }

    @Test
    @Parameters(value = {"4", "5"})
    public void given_invalidDirectionType_when_getByValueIsInvoked_then_returnEmpty(int value) {
        // When
        Optional<DirectionType> result = DirectionType.getByValue(value);

        // Then
        assertThat(result).isEmpty();
    }
}