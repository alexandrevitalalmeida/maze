package com.task.maze;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MazeCellTypeTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    @Parameters(value = {"X", " ", "S", "F"})
    public void given_validCellValue_when_getByValueIsInvoked_then_returnMazeCellType(String value) {
        // When
        Optional<MazeCellType> cellType = MazeCellType.getByValue(value);

        // Then
        assertThat(cellType).isPresent();
    }

    @Test
    @Parameters(value = {"A", "B", "null"})
    public void given_invalidCellValue_when_getByValueIsInvoked_then_returnEmpty(String value) {
        // When
        Optional<MazeCellType> somethingInvalid = MazeCellType.getByValue(value);

        // Then
        assertThat(somethingInvalid).isNotPresent();
    }
}