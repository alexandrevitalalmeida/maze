package com.task.maze;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MazePositionTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    @Parameters(value = {"RIGHT", "LEFT"})
    public void given_invalidDirectionTypeForRows_when_nextRowIsInvoked_then_rowIndexRemains(DirectionType directionType) {
        // Given
        MazePosition mazePosition = new MazePosition(3, 3);

        // When
        int nextRow = mazePosition.getNextRow(directionType);

        // Then
        assertThat(nextRow).isEqualTo(3);
    }

    @Test
    @Parameters(value = {"UP", "DOWN"})
    public void given_invalidDirectionTypeForColumns_when_nextColumnIsInvoked_then_columnIndexRemains(DirectionType directionType) {
        // Given
        MazePosition mazePosition = new MazePosition(3, 3);

        // When
        int nextColumn = mazePosition.getNextColumn(directionType);

        // Then
        assertThat(nextColumn).isEqualTo(3);
    }

    @Test
    public void given_validDirectionUpForRow_when_nextRowIsInvoked_then_returnValue() {
        // Given
        MazePosition mazePosition = new MazePosition(5, 5);

        // When
        int nextRow = mazePosition.getNextRow(DirectionType.UP);

        // Then
        assertThat(nextRow).isEqualTo(4);
    }

    @Test
    public void given_validDirectionDownForRow_when_nextRowIsInvoked_then_returnValue() {
        // Given
        MazePosition mazePosition = new MazePosition(5, 5);

        // When
        int nextRow = mazePosition.getNextRow(DirectionType.DOWN);

        // Then
        assertThat(nextRow).isEqualTo(6);
    }

    @Test
    public void given_validDirectionRightForColumn_when_nextColumnIsInvoked_then_returnValue() {
        // Given
        MazePosition mazePosition = new MazePosition(2, 2);

        // When
        int nextColumn = mazePosition.getNextColumn(DirectionType.RIGHT);

        // Then
        assertThat(nextColumn).isEqualTo(3);
    }

    @Test
    public void given_validDirectionLeftForColumn_when_nextColumnIsInvoked_then_returnValue() {
        // Given
        MazePosition mazePosition = new MazePosition(2, 2);

        // When
        int nextColumn = mazePosition.getNextColumn(DirectionType.LEFT);

        // Then
        assertThat(nextColumn).isEqualTo(1);
    }
}